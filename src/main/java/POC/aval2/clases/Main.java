package POC.aval2.clases;

import java.util.ResourceBundle;
/**
 * Hello world!
 *
 */
public class Main 
{
	public static String configFile;
	
	public static ResourceBundle prop;
	public Utils ut;
	public String url;
	
    public static void main( String[] args )
    {
    	Main mn=new Main();
    	mn.iniciarAplicacion();
    }
    
    public Main() {
    	configFile="main";
    	
    	ut=new Utils();
    	prop=ut.cargarDos(configFile);
    }
    
   
    
    public void iniciarAplicacion() {
    	ut.startURL(prop.getString("url").replace("\"", ""));
    }
}

